#!/usr/bin/python3
# coding=utf-8
import json
import zipfile
import os

class SimpleZip:
    def __init__(self, zipf):
        self.zip = zipf

    def addfile(self, file, arc=None):
        print(file, arc)
        self.zip.write(file, arc)

    def adddir(self, path):
        for root, dirs, files in os.walk(path):
            for file in files:
                self.addfile(os.path.join(root, file))


if __name__ == '__main__':
    manifest = json.load(open("manifest.json"))
    Zip = SimpleZip(zipfile.ZipFile(manifest["name"] + " V" + manifest["version"] + ".zip", 'w'))
    Zip.addfile("LICENSE.txt")
    Zip.addfile("manifest.json")
    Zip.addfile("modlist.html")
    Zip.adddir("overrides")
    Zip.zip.close()

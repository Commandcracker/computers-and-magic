import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.jei.JEI;

print("Script starting!");

var itemArray = [
    <tconstruct:slimesling:0>,
    <tconstruct:slimesling:1>,
    <tconstruct:slimesling:2>,
    <tconstruct:slimesling:3>,
    <tconstruct:slimesling:4>,
    <tconstruct:slime_boots:0>,
    <tconstruct:slime_boots:1>,
    <tconstruct:slime_boots:2>,
    <tconstruct:slime_boots:3>,
    <tconstruct:slime_boots:4>,
    <buildcraftbuilders:quarry>
] as IItemStack[];

for item in itemArray {
    JEI.removeAndHide(item);
}

print("Script ending!");
